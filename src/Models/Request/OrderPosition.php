<?php

namespace Oskonnikov\Alfabank\Models\Request;

use Oskonnikov\Alfabank\Models\BankModel;

class OrderPosition extends BankModel
{
	protected $guarded = [
		'amount'
	];

	public $id;

	public $amount;

	/**
	 * НДС 20%
	 */
	const TAX_TYPE = 6;

	public $name;

	public $tax;

	public $quantity;

	public $description;

	public $measure;

	public function __construct($id, $name, $amount, $description, $tax = self::TAX_TYPE, $quantity = 1, $measure = 'штук')
	{
		$this->id = $id;
		$this->amount = ((float)$amount) * Order::AMOUNT_RATIO;
		$this->name = $name;
		$this->description = $description;
		$this->tax = $tax;
		$this->quantity = $quantity;
		$this->measure = $measure;
	}
}
