<?php

namespace Oskonnikov\Alfabank\Models\Response;

use Oskonnikov\Alfabank\Models\BankModel;

class PaymentUrlResponse extends BankModel
{
	public $error;

	public $paymentUrl;

	public $bankOrderId;

	public $errorCode;

	public $errorMessage;

	public $jsonResponse;
}
